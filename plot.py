#!/usr/bin/python
import pylab
import numpy as np
import code
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

def plot_xs(path,nplots=1000,nplot=None,scale=1,*args,**kwargs):
    file = open(path)
    data = [map(float,line.split()) for line in file]
    datasets = [[],[],[]]
    counter = -1
    for line in data:
        print line
        if line[0]==0.0:
            counter += 1
            print counter
        datasets[counter].append(line)
    datasets = np.asarray(datasets)
    file.close()


    if nplot != None:
        pylab.plot(datasets[nplot][:,0],[x*scale for x in datasets[nplot][:,1]],*args,**kwargs)
    else:
        for i in range(0,counter+1):
            if i == nplots:
                break
            pylab.plot(datasets[i][:,0],[x*scale for x in datasets[i][:,1]],*args,**kwargs)
    pylab.ylabel("d$\sigma$/d$\Omega$ (mb/sr)")
    pylab.xlabel("Theta (degrees)")
    pylab.legend()


def plot_wvfunc(path):
    file = open(path)
    data = [map(float,line.split()) for line in file]
    datasets = [[],[],[],[]]
    counter = -1
    for line in data:
        if len(line)==4:
            counter += 1
            print counter
            continue
        if len(line)!=5:
            continue
        datasets[counter].append(line)
    datasets = np.asarray(datasets)
    wfset = []
    for i in range(0,len(datasets)):
        temp = np.asarray(datasets[i])
        wf = []
        for j in range(0,len(temp[0])):
            for el in temp[:,j]:
                wf.append(el)
        wfset.append(wf)

    radius = np.arange(0.1,15.1,0.1)
    wfset = np.asarray(wfset)
    fig = pylab.figure(figsize=(5,2))
    canvas = pylab.subplot(1,1,1)
    print len(radius),len(wfset[0])
    pylab.plot(radius,wfset[0],label="0")
    pylab.plot(radius,wfset[1],label="1")
    pylab.plot(radius,wfset[2],label="2")
    pylab.plot(radius,wfset[3],label="3")
    pylab.legend()
    pylab.xlim(0.1,2.2)
    pylab.savefig('/user/sullivan/public_html/research/wvfunc2.pdf')
    #pylab.show()
    code.interact(local=locals())

def wood_saxon(V,r,a,Ap,At):
    domain = np.arange(0.0,10.1,0.1)
    mass = np.power(Ap,1.0/3.0)+np.power(At,1.0/3.0)
    exp = np.exp((domain-r*mass)/a)
    output = -np.abs(V)/(1+exp)
    return [domain,output]

def wood_saxon2(V,r,a,Ap,At):
    domain = np.arange(0.0,10.1,0.1)
    mass = np.power(At,1.0/3.0)
    exp = np.exp((domain-r*mass)/a)
    output = -np.abs(V)/(1+exp)
    return [domain,output]

def plot_pot(V,r,a,Ap,At,idxmass=0,**kwargs):
    if idxmass==0:
        radius, pot = wood_saxon(V,r,a,Ap,At)
    else:
        radius, pot = wood_saxon2(V,r,a,Ap,At)
    pylab.plot(radius,pot,**kwargs)


if __name__=='__main__':
    fig = pylab.figure(figsize=(8.25,5))
    canvas = pylab.subplot(1,1,1)
    #canvas.set_yscale('log')


    # all fe56 states
    #for i in range(0,10):
    #   plot_xs("./fe56all/state{0}/dwhi.plot".format(i),nplot=0)
    #plot_xs("./dwhi.plot",nplot=0)
    #plot_xs("./fe56all/state0/dwhi.plot",nplot=0)

    # fe56 T=3 states comparison
    #
    #plot_xs("./fe56fe56_t3_tz2/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,2>$_f$")
    #plot_xs("./fe56fe56_t3_tz1/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,1>$_f$")

    #plot_xs("./fe56fe56_t3_tz2_q0/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,2>$_f$")
    #plot_xs("./fe56fe56_t3_tz1_q0/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,1>$_f$")
    ##plot_xs("./fe56fe56_t2_tz1_q0/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |2,1>$_f$")

    # ce w/ inelastic densities and coloumb
    #plot_xs("./fe56fe56_t3_tz1_q0/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,1>$_f$")
    #plot_xs("./fe56fe56_t3_tz1_q0_w_inel_tz_coloumb_dens/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,2>$_f$")
    #pylab.title('Q=0')

    # ce w/ inelastic coloumb
    #plot_xs("./fe56fe56_t3_tz1_q0/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,1>$_f$")
    #plot_xs("./fe56fe56_t3_tz1_q0_w_inel_coloumb/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,1>$_f$ inel. coloumb")
    #pylab.title('Q=0')

    # ce w/ inelastic dens
    #plot_xs("./fe56fe56_t3_tz1_q0/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,1>$_f$")
    #plot_xs("./fe56fe56_t3_tz1_q0_w_inel_dens/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,1>$_f$ inel. dens.")
    #pylab.title('Q=0')

    # ce w/ inelastic isospin
    #plot_xs("./fe56fe56_t3_tz1_q0/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,1>$_f$")
    #plot_xs("./fe56fe56_t3_tz1_q0_w_inel_tz/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,1>$_f$ inel. tz.")
    #pylab.title('Q=0')

    # inelastic
    #plot_xs("./fe56fe56_t3_tz1_q0/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,1>$_f$")
    #plot_xs("./fe56fe56_t3_tz1_q0_w_inel_tz_coloumb_dens/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,2>$_f$")
    #pylab.title('Q=0')

    # inelastic comparison with scaled ce
    #plot_xs("./fe56fe56_t3_tz1_q0/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,1>$_f \beta^-$ CE ")
    #plot_xs("./fe56fe56_t3_tz1_q0_w_inel_tz_coloumb_dens/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |3,2>$_f$ In.",color='r')
    #plot_xs("./fe56fe56_t3_tz1_q0_w_inel_dens_coloumb/dwhi.plot",nplot=0,scale=5,label="T: |2,2>$_i$ $\longrightarrow$ |3,1>$_f \beta^-$ CE x5",linestyle='--',color='k')
    #pylab.title('Q=0')

    # fe56 T=2 states
    #
    #plot_xs("./fe56fe56_t2_tz1_q0/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |2,1>$_f \beta^-$ CE ")
    #plot_xs("./fe56fe56_t2_tz2_q0_w_ce_tz/dwhi.plot",nplot=0,scale=1,label="T: |2,2>$_i$ $\longrightarrow$ |2,1>$_f \beta^-$ w/ inel. dens. & coloumb",color='r')
    #plot_xs("./fe56fe56_t2_tz1_q0_w_inel_dens/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |2,2>$_f$ w/ inel dens",color='r')
    #plot_xs("./fe56fe56_t2_tz1_q0_w_inel_coulomb/dwhi.plot",nplot=0,label="T: |2,2>$_i$ $\longrightarrow$ |2,2>$_f$ w/ inel couloumb",color='r')
    #plot_xs("./fe56fe56_t2_tz2_q0_w_ce_tz/dwhi.plot",nplot=0,scale=2,label="T: |2,2>$_i$ $\longrightarrow$ |2,2>$_f$ Inelastic",linestyle='--',color='k')
    #pylab.title('Q=0')

    # plot_xs("./test_full_dens/dwhi.plot",nplot=0,label="full dens")
    # plot_xs("./test_truncated_dens/dwhi.plot",nplot=0,label="truncated dens")
    # plot_xs("./dwhi.plot",nplot=0,label="ebind")
    #plot_xs("./dwhi.plot",nplot=0,label="c12")



    # mg24
    #plot_xs("./mg24mg24_sk20_li6li6_sk6_OMP_kim1/dwhi.plot",nplot=1,color='r')
    #plot_xs("./mg24mg24_prunesk20_li6li6_datask6_OMP_kim1/dwhi.plot",nplot=1,color='k')
    #plot_xs("./mg24mg24_prunesk20_li6li6_datask6_OMP_kim2/dwhi.plot",nplot=1,color='b')

    # all mg24 states
    # for i in range(0,10):
    #     plot_xs("./mg24all/state{0}/dwhi.plot".format(i),nplot=0)

    # c12
    # plot_xs("./c12c12_sk12_li6li6_datask6/dwhi.plot",nplot=0,color='b')
    # plot_xs("./c12n12_sk12_li6he6_sk6/dwhi.plot",nplot=0,color='r')
    # plot_xs("./c12n12_sk12_li6he6_datask6/dwhi.plot",nplot=0,color='k')
    # plot_xs("./c12c12_sk12_li6li6_datask6/dwhi.plot",nplot=1,color='b')
    plot_xs("./c12c12_sk12_li6li6_datask6_schwartz/dwhi.plot",nplot=0,color='k')
    plot_xs("./c12c12_sk12_li6li6_datask6_schwartz2/dwhi.plot",nplot=0,color='b')
    plot_xs("./c12c12_sk12_li6li6_sk6_schwartz/dwhi.plot",nplot=0,color='r')

    # all c12 states
    #for i in range(0,8):
    #    plot_xs("./c12all/state{0}/dwhi.plot".format(i),nplot=0)


    pylab.show()
    #pylab.savefig('xs.pdf')


    # fig = pylab.figure(figsize=(5,2.75))
    # canvas = pylab.subplot(1,1,1)
    #Nadasen
    #plot_pot(-126.9,1.136,0.897,0,12.0)#6Li+12C - Real
    #plot_pot(-29.3,1.695,0.878,0,12.0)#6Li+12C - Imaginary
    #Anataramen
    #plot_pot(-120,0.71,0.840,6,12.0,color='k',linestyle='--')#12C+12C - Real
    #plot_pot(-34,0.96,0.690,6,12.0,color='r',linestyle='--')#12C+12C - Imaginary
    #Schwartz fit1 - 600MeV
    #plot_pot(-60.938015,1.372525,0.914180,6,12.0,idxmass=1,color='b',linestyle='--')#6Li+12C - Real
    #plot_pot(-22.529179,1.609820,0.692986,6,12.0,idxmass=1,color='g',linestyle='--')#6Li+12C - Imaginary
    #Schwartz fit2 - 600MeV
    #plot_pot(-62.052826,1.354315,0.932602,6,12.0,'k--')#6Li+12C - Real
    #plot_pot(-27.047440,1.439350,0.855838,6,12.0,'r--')#6Li+12C - Imaginary
    #Schwartz fe56 fit1
    #plot_pot(-122.88,1.08,1.0624,6,56.0,idxmass=1,color='k')#6Li+56Fe Real
    #plot_pot(-59.293,1.377,0.730,6,56.0,idxmass=1,color='r')#6Li+56Fe Imaginary



    #pylab.xlim(0,10)
    #pylab.ylim(-70,10)
    #pylab.savefig('pots.pdf')
